#!/usr/bin/env python

import unittest

import equinox as eqx
import jax
import jax.numpy as jnp
import numpy as np

import qcd

class TestRandom(unittest.TestCase):
    def test_haar_unitary(self):
        ukey = jax.random.PRNGKey(0)
        U = qcd.haar_unitary(ukey, 3)
        I = U.conj().transpose() @ U
        self.assertAlmostEqual(jnp.sum(jnp.abs(I-jnp.eye(3))), 0., places=5)

    def test_haar_random(self):
        ukey = jax.random.PRNGKey(0)
        K = 100000
        ukeys = jax.random.split(ukey, K)
        U = jax.vmap(lambda k: qcd.haar_unitary(k, 3))(ukeys)
        trace = jnp.trace(U,axis1=1,axis2=2)
        self.assertLessEqual(jnp.abs(jnp.mean(trace)), 5*jnp.std(trace)/jnp.sqrt(K))

class TestAlgebra(unittest.TestCase):
    def test_hermiticity(self):
        for N in range(2,8):
            algebra = qcd.SpecialUnitaryAlgebra(N)
            v = algebra(jax.random.uniform(jax.random.PRNGKey(0),shape=(N**2-1,)))
            vdag = v.conj().transpose()
            self.assertAlmostEqual(jnp.sum(jnp.abs(v-vdag)), 0., places=5)

    def test_orthonormality(self):
        for N in range(2,8):
            algebra = qcd.SpecialUnitaryAlgebra(N)
            for i,v in enumerate(algebra.basis):
                for j,w in enumerate(algebra.basis):
                    tr = (v @ w).trace()
                    if i == j:
                        self.assertAlmostEqual(tr, 1., places=5)
                    else:
                        self.assertAlmostEqual(tr, 0., places=5)

    def test_structure(self):
        for N in range(2,8):
            algebra = qcd.SpecialUnitaryAlgebra(N)
            for a,Ta in enumerate(algebra.basis):
                for b,Tb in enumerate(algebra.basis):
                    H = np.zeros_like(Ta)
                    H_ = Ta@Tb - Tb@Ta
                    for c,Tc in enumerate(algebra.basis):
                        H += algebra.structure[a,b,c]*Tc
                    self.assertAlmostEqual(np.sum(np.abs(H-H_)), 0., places=5)

    def test_vector(self):
        for N in range(2,8):
            algebra = qcd.SpecialUnitaryAlgebra(N)
            v = jax.random.uniform(jax.random.PRNGKey(0),shape=(N**2-1,))
            self.assertAlmostEqual(jnp.sum(jnp.abs(v-algebra.vector(algebra(v)))), 0, places=5)

class TestSmallExactForm(unittest.TestCase):
    def test_init(self):
        ukey, fkey = jax.random.split(jax.random.PRNGKey(0))
        form = qcd.ExactForm(fkey,2,2,1,zero=True)
        U = qcd.random_unitary(ukey, 3, 1.)
        self.assertLess(jnp.sum(jnp.abs(form.vector(U))), 1e-5)

    def test_nonzero(self):
        ukey, fkey = jax.random.split(jax.random.PRNGKey(0))
        form = qcd.ExactForm(fkey,2,2,1,zero=False)
        U = qcd.random_unitary(ukey, 3, 1.)
        self.assertNotEqual(jnp.sum(jnp.abs(form.vector(U))), 0)
        self.assertNotEqual(jnp.abs(form(U)), 0)

    def test_integral_su2(self):
        N = 2
        ukey, fkey = jax.random.split(jax.random.PRNGKey(0))
        form = qcd.ExactForm(fkey,2,2,1,N=N,zero=False)
        K = 100000
        fs = []
        for batch in range(100):
            ukey, ukey_ = jax.random.split(ukey)
            ukeys = jax.random.split(ukey_, K)
            U = jax.vmap(lambda k: qcd.haar_unitary(k, N))(ukeys)
            f = jax.vmap(eqx.filter_jit(form))(U)
            fs.append(jnp.mean(f))
        fs = np.array(fs)
        mean, std = qcd.bootstrap(fs)
        self.assertLessEqual(mean, 5*std)

    def test_integral_su3(self):
        N = 3
        ukey, fkey = jax.random.split(jax.random.PRNGKey(0))
        form = qcd.ExactForm(fkey,2,2,1,N=N,zero=False)
        K = 10000
        fs = []
        for batch in range(100):
            ukey, ukey_ = jax.random.split(ukey)
            ukeys = jax.random.split(ukey_, K)
            U = jax.vmap(lambda k: qcd.haar_unitary(k, N))(ukeys)
            f = jax.vmap(eqx.filter_jit(form))(U)
            fs.append(jnp.mean(f))
        fs = np.array(fs)
        mean, std = qcd.bootstrap(fs)
        self.assertLessEqual(mean, 5*std)

    def test_big_integral(self):
        N = 3
        ukey, fkey = jax.random.split(jax.random.PRNGKey(0))
        form = qcd.ExactForm(fkey,2,2,2,N=N,zero=False)
        K = 10000
        fs = []
        for batch in range(100):
            ukey, ukey_ = jax.random.split(ukey)
            ukeys = jax.random.split(ukey_, K)
            def mkUs(k):
                k, k_ = jax.random.split(k)
                return jnp.array([qcd.haar_unitary(k,N), qcd.haar_unitary(k_,N)])
            U = jax.vmap(mkUs)(ukeys)
            f = jax.vmap(eqx.filter_jit(form))(U)
            fs.append(jnp.mean(f))
        fs = np.array(fs)
        mean, std = qcd.bootstrap(fs)
        self.assertLessEqual(mean, 5*std)

class TestSmallScaledExactForm(unittest.TestCase):
    def setUp(self):
        pass

    def test_init(self):
        pass

    def test_integral(self):
        pass

class TestLattice(unittest.TestCase):
    def setUp(self):
        pass

    def test_loop(self):
        pass

    def test_random(self):
        pass

class TestStaggered(unittest.TestCase):
    def setUp(self):
        lattice = qcd.Lattice(4)

    def test_zero_density(self):
        pass

    def test_saturation_density(self):
        pass

    def test_bare_mass_density(self):
        pass

if __name__ == '__main__':
    unittest.main()

