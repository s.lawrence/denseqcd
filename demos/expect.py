#!/usr/bin/env python

import sys
from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax

#import mc.metropolis

jax.config.update('jax_platform_name', 'cpu')

V = 2
eps = 0.08

@eqx.filter_jit
def f(theta):
    return jnp.prod(jnp.cos(theta) + eps)

@eqx.filter_jit
def S(theta):
    return -jnp.log(f(theta)+0j)

class ExactForm(eqx.Module):
    """ An exact, top-rank differential form.
    """
    D: int

    def __init__(self, key, D):
        self.D = D
        
    def _v(self, x):
        return jnp.array([jnp.sin(x[0])*jnp.cos(x[1]) + 1.2*eps*jnp.sin(x[0]),0.7*eps*jnp.sin(x[1])])

    def __call__(self, x):
        j = jax.jacfwd(self._v)(x)
        dv = jnp.sum(jnp.diagonal(j))
        return dv

chainKey, formKey = jax.random.split(jax.random.PRNGKey(0))

form = ExactForm(formKey, V)

@eqx.filter_jit
def Seff(form, theta):
    return -jnp.log(f(theta) - form(theta) + 0j)

@eqx.filter_jit
@eqx.filter_grad
def Seff_grad(form, theta):
    return jnp.log(f(theta) - form(theta) + 0j).real

opt = optax.yogi(1e-2)
#opt = optax.sgd(1e-3)
opt_state = opt.init(form)

# Collect a bunch of sample points
fs = []
forms = []
Ss = []
xs = []
#print('Sampling...')
for _ in range(10000):
    x = jnp.array(np.random.uniform(size=(V,))*2*np.pi)
    xs.append(x)
    Ss.append(Seff(form,x))
    fs.append(f(x))
    forms.append(eqx.filter_jit(form)(x))

boltz = [jnp.exp(-S) for S in Ss]
print(f'Average sign: {(np.mean(boltz)/np.mean(np.abs(boltz))).real}')

#num = [fx*jnp.cos(x[0]) - formx for x,fx,formx in zip(xs,fs,forms)]
num = [fx*jnp.cos(x[0]) for x,fx,formx in zip(xs,fs,forms)]

print(np.mean(num)/np.mean(boltz))
print(np.mean(num)/np.mean(fs))

