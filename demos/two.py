#!/usr/bin/env python

import sys
from typing import Callable

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax

#import mc.metropolis

jax.config.update('jax_platform_name', 'cpu')

class MLP(eqx.Module):
    activation: Callable = staticmethod(jax.nn.selu)
    layers: list

    def __init__(self, key, width):
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    def __call__(self, x):
        act = self.activation
        for layer in self.layers[:-1]:
            x = act(layer(x))
        return self.layers[-1](x)

V = 2
eps = 0.1

def f(theta):
    return jnp.prod(jnp.cos(theta) + eps)

def S(theta):
    return -jnp.log(f(theta)+0j)

class ExactForm(eqx.Module):
    """ An exact, top-rank differential form.
    """
    D: int
    mlp: MLP

    def __init__(self, key, D):
        self.D = D
        # TODO
        #self.mlp = MLP(key, [2*D,2*D,2*D,D])
        self.mlp = MLP(key, [2*D,D])
        
    def _v(self, x):
        # TODO
        #return self.mlp(jnp.concatenate([jnp.cos(x), jnp.sin(x)]))
        #return self.mlp(jnp.array([1., jnp.cos(x[0]), jnp.cos(x[1]), jnp.cos(x[0])*jnp.cos(x[1])]))
        #return jnp.array([jnp.sin(x[0])*jnp.cos(x[1])+eps*jnp.sin(x[0]), eps*jnp.sin(x[1])])
        return self.mlp(jnp.array([jnp.sin(x[0]), jnp.sin(x[1]), jnp.cos(x[0])*jnp.sin(x[1]), jnp.sin(x[0])*jnp.cos(x[1])]))

    def __call__(self, x):
        j = jax.jacfwd(self._v)(x)
        dv = jnp.sum(jnp.diagonal(j))
        #return jnp.cos(x[0])*jnp.cos(x[1])
        #return jnp.cos(x[0])*jnp.cos(x[1]) + eps*jnp.cos(x[0]) + eps*jnp.cos(x[1])
        return dv

chainKey, formKey = jax.random.split(jax.random.PRNGKey(0))

form = ExactForm(formKey, V)

@eqx.filter_jit
def Seff(form, theta):
    return -jnp.log(f(theta) - form(theta) + 0j)

@eqx.filter_jit
@eqx.filter_grad
def Seff_grad(form, theta):
    return jnp.log(f(theta) - form(theta) + 0j).real

opt = optax.yogi(1e-2)
#opt = optax.sgd(1e-3)
opt_state = opt.init(form)

if False:
    theta0 = jnp.zeros((V,))
    chain = mc.metropolis.Chain(lambda x: Seff(form,x), theta0, chainKey)
while True:
    if False:
        chain.calibrate()

    # Collect a bunch of sample points
    Ss = []
    xs = []
    #print('Sampling...')
    for _ in range(100):
        x = jnp.array(np.random.uniform(size=(V,))*2*np.pi)
        xs.append(x)
        Ss.append(Seff(form,x))

    for _ in range(1):
        S_s = [Seff(form, x) for x in xs]
        boltz = [jnp.exp(-S_) for S_ in S_s]
        print(f'Average sign: {(np.mean(boltz)/np.mean(np.abs(boltz))).real}')

        # For each sample, compute the gradient
        grads = []
        for x in xs:
            boltz = jax.jit(lambda x: jnp.abs(f(x)-form(x)))(x)
            grads.append(jax.tree_map(lambda x: boltz*x, Seff_grad(form, x)))

        # Update
        grad = jax.tree_map(lambda *x: jnp.mean(jnp.array(x),axis=0), *grads)
        print(grad.mlp.layers[0].weight)
        updates, opt_state = jax.jit(opt.update)(grad, opt_state)
        form = eqx.filter_jit(eqx.apply_updates)(form, updates)

