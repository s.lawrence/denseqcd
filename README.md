# Dense QCD on the lattice

## Papers

This code is heavily based on the following papers:

* [Finite-Density Monte Carlo Calculations on Sign-Optimized Manifolds](https://arxiv.org/abs/1804.00697)
* [Perturbative Removal of a Sign Problem](https://arxiv.org/abs/2009.10901)

