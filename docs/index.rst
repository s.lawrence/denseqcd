.. Dense QCD documentation master file, created by
   sphinx-quickstart on Mon Jun 27 16:13:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dense QCD's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   quickstart
   cli
   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

