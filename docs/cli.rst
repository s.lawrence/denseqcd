Command-line interface
======================

Fermions
--------

Subtraction
-----------

Training
--------

Other Options
-------------

Physics
^^^^^^^

* ``-N N_C`` (``--colors N_C``): simulate SU(N_C) gauge fields

Random seed
^^^^^^^^^^^

By default, the random number generator is initialized with a seed of ``0``. This behavior may be modified with:

* ``--seed SEED``: seed the PRNG to the integer ``SEED``.
* ``--seed-time``: seed the PRNG using the current time.

Logging
^^^^^^^

Unless otherwise specified, all log output is written to standard error.
Logging behavior may be tweaked with the following flags:

* ``-v`` (or ``--verbose``): increase the detail at which logging messages are output.

