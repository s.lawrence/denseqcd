qcd module
==========

.. automodule:: qcd
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource
